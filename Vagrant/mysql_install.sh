#!/bin/bash

RED="\e[31m"
GREEN="\e[32m"
ENDCOLOR="\e[0m"

echo -e "${GREEN}Set MySQL Pass${ENDCOLOR}"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password password"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password password"

echo -e "${GREEN}Install MySQL${ENDCOLOR}"
apt-get -y install mysql-server

systemctl start mysql.service
systemctl enable mysql.service

echo -e "${GREEN}Open connections from the outside${ENDCOLOR}"
sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf

echo -e "${RED}Create developer user for MySQL${ENDCOLOR}"
mysql -u'root' -p'password' -t << EOF
CREATE USER "dev_user"@"%" IDENTIFIED BY "dev-password";
GRANT CREATE, ALTER, DROP, INSERT, UPDATE, DELETE, SELECT, REFERENCES, RELOAD, INDEX on *.* TO 'dev_user'@'%';
FLUSH PRIVILEGES;
SELECT user,host FROM mysql.user;
SHOW GRANTS FOR dev_user; 
EOF

service mysql restart
